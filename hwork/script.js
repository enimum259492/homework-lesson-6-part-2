function Human(name,surname,country){
    this.name = name,
        this.surname = surname,
        this.country = country,


        this.info = function (){
            console.log(`Hey! Wassup, It's me ${this.name} ${this.surname}, im from ${this.country}`)
        }
}

Human.prototype.likesDogs = function (){
    console.log(`And I like dogs`)
}

const someGuy = new Human("Max","wachowski","Poland")
someGuy.info();
someGuy.likesDogs();

const someGirl = new Human("Ann", "Nikiforova","Russia")
someGirl.info();
someGirl.likesDogs();



